# Contributor Code of Conduct


Our [company values](https://www.arcticshores.com/our-story) guide us in our day-to-day interactions and decision-making. Our community welcomes participants from around the world with different experiences, unique perspectives, and great ideas to share.

We are committed to providing a friendly, safe and welcoming environment for all, regardless of age, body size, disability, ethnicity, sex characteristics, gender identity and expression, level of experience, education, socio-economic status, nationality, personal appearance, race, religion, sexual identity and orientation, or other such characteristics.


## Our Standards

Diversity is one of our huge strengths, but it can also lead to communication issues and unhappiness. To that end, we have a few ground rules that we ask people to adhere to. This code applies equally to founders, mentors and those seeking help and guidance.

This isn't an exhaustive list of things that you can't do. Rather, take it in the spirit in which it's intended - a guide to make it easier to enrich all of us and the technical communities in which we participate.


### Encouraged

- **Be kind and courteous.** We treat our fellow community members with the empathy, respect and dignity all humans deserve. Keep in mind that public communication is received by many people you don't know, so before sending a message please ask yourself whether someone from a different context would misunderstand it.
- **Respect differences of opinion and remember that every design or implementation choice carries a trade-off and numerous costs.** There is seldom a single right answer; we will find the best solutions by engaging in constructive discussion, with everybody bringing their unique viewpoint and experience to the table.
- **Remember that one of our core values is seeing the potential in people.** Everyone was inexperienced at some point. We want to encourage newcomers to join our community irrespective of experience background. Always assume good intentions and a willingness to learn, just as you are willing to evolve your own opinion as you gain new insights.


### Discouraged

- Keep unstructured critique to a minimum. We encourage sharing ideas and perspectives, so please ensure that your feedback is constructive and relevant. If you have solid ideas you want to experiment with, make a fork and see how it works.
- Avoid aggressive and micro-aggressive behavior, such as unconstructive criticism, providing corrections that do not improve the conversation, repeatedly interrupting or talking over someone else, feigning surprise at someone's lack of knowledge or awareness about a topic, or subtle prejudice (for example, comments like "That's so easy my grandmother could do it."). For more examples of this kind of behavior, see the Recurse Center's user manual.
- We will exclude you from interaction if you insult, demean or harass anyone. See examples of unacceptable behavior below. In particular, we don't tolerate behavior that excludes people in socially marginalized groups.
- Private harassment is also unacceptable. No matter who you are, if you feel you have been or are being harassed or made uncomfortable by a community member's behavior, please contact the moderation team immediately.
- Likewise any spamming, trolling, flaming, baiting or other attention-stealing behavior is not welcome.
- Claiming that you are excercising your free speech right while not following our Code of Conduct; we have chosen to create this project for technical expression and not as a platform for you to exercise what you feel counts as free speech. If you need a platform for free speech, please find a more suitable project.


#### Examples of unacceptable behavior


- Violence, threats of violence or violent language directed against another person.
- Sexist, racist, homophobic, transphobic, ableist or otherwise discriminatory jokes and language.
- Posting or displaying sexually explicit or violent material.
- Posting or threatening to post other people’s personally identifying information (“doxing”).
- Personal insults, particularly those related to gender, sexual orientation, race, religion, or disability.
- Inappropriate photography or recording.
- Inappropriate physical contact. You should have someone’s consent before touching them.
- Unwelcome sexual attention. This includes, sexualized comments or jokes; inappropriate touching, groping, and unwelcomed sexual advances.
- Deliberate intimidation, stalking or following (online or in person).
- Advocating for, or encouraging, any of the above behavior.
- Sustained disruption of community events, including talks and presentations.


## Moderation

These are the policies for upholding our community’s standards of conduct. If you feel that a thread needs moderation, [please contact the moderation team](#contact).

- Remarks that violate the above code of conduct, including hateful, hurtful, oppressive, or exclusionary remarks, are not allowed. (Cursing is allowed, but never targeting another user, and never in a hateful or aggressive manner.)
- Moderators will warn users who make remarks inconsistent with the above code of conduct.
- If the warning is unheeded, the user will be banned temporarily from the communication channel to cool off.
- If the user comes back and continues to make trouble, they will be banned indefinitely.
- Moderators may choose at their discretion to un-ban the user if it was a first offense and they if they make suitable amends with the offended party.
- If you think a moderator action is unjustified, please take it up with that moderator, or with a different moderator, in private. Complaints about moderation in-channel are not allowed.
- Moderators are held to a higher standard than other community members. If a moderator acts inappropriately, they should expect less leeway than others.


## Contact

For CoC-related questions or to report possible violations on the channels listed above,

- contact one of the moderators active on that channel if you can identify them, or
- send an e-mail to [open-source@arcticshores.com](mailto:open-source@arcticshores.com)


## Credits

The Arctic Shores Code of Conduct was adapted from the [Scala Code of Conduct](https://www.scala-lang.org/conduct/#contact) and a few others:

- [Citizen Code of Conduct](https://web.archive.org/web/20200330154000/http://citizencodeofconduct.org/)
- [Lighbend Code of Conduct](https://www.lightbend.com/conduct)
- [auth0](https://github.com/auth0/open-source-template/blob/master/CODE-OF-CONDUCT.md)
