#https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/PostgreSQL-Lambda.html


resource "aws_db_instance_role_association" "pg_invoke_lambda" {
  db_instance_identifier = var.db_instance_identifier
  feature_name           = "Lambda"
  role_arn               = aws_iam_role.pg_rds_to_lambda_role.arn
}

resource "aws_iam_policy" "pg_invoke_lambda" {
  name        = "${var.descriminator}_pg_invoke_lambda"
  description = "IAM policy for allowing lambda invocation"

  policy = jsonencode(
    {
      Version = "2012-10-17"
      Statement = [
        {
          Action = [
            "lambda:InvokeFunction"
          ]
          Resource = var.lambda_arn
          Effect   = "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_role" "pg_rds_to_lambda_role" {
  name               = "${var.descriminator}_pg_rds_to_lambda_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "rds.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.pg_rds_to_lambda_role.name
  policy_arn = aws_iam_policy.pg_invoke_lambda.arn
}