variable "lambda_arn" {
  type        = string
  description = "The lambda to be invoked by rds"
}

variable "db_instance_identifier" {
  type        = string
  description = "The rds to invoke a given lambda"
}

variable "security_group_id" {
  type        = string
  description = "A user defined security group for the lambda"
  default     = null
}

variable "descriminator" {
  type        = string
  description = "A prefix for resources to be namespaced"
}