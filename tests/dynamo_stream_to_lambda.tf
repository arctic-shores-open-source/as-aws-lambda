resource "aws_dynamodb_table" "dynamodb_test" {
  name             = "dynamodb_test"
  billing_mode     = "PAY_PER_REQUEST"
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"
  hash_key         = "test"
  attribute {
    name = "test"
    type = "S"
  }
}

module "dynamodb_processor" {
  source         = "../lambda_dynamodb_stream"
  lambda_name    = "dynamo_to_lambda_test"
  lambda_handler = "lambda_dynamodb_stream.handler"
  lambda_environment_variables = {
    MY_ENV_VARIABLE = "my value"
  }
  lambda_service_subnets         = [aws_subnet.main.id]
  lambda_source_dir              = "lambdas/dummy"
  vpc_id                         = aws_vpc.main.id
  dead_letters_queue_name        = "dynamo_to_lambda_failures"
  event_source_starting_position = "LATEST"
  enable_consumer                = true
  aws_dynamodb_stream_arn        = aws_dynamodb_table.dynamodb_test.stream_arn
}


