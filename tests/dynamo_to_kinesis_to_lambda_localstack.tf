provider "aws" {
  version                     = "4.34.0"
  region                      = "eu-west-1"
  access_key                  = "foo"
  secret_key                  = "bar"
  skip_credentials_validation = true
  skip_metadata_api_check     = true
  skip_requesting_account_id  = true

  endpoints {
    dynamodb = var.localstack_endpoint
    kinesis  = var.localstack_endpoint
    lambda   = var.localstack_endpoint
    iam      = var.localstack_endpoint
    sns      = var.localstack_endpoint
    ec2      = var.localstack_endpoint
  }
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "main" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "Main"
  }
}


resource "aws_kinesis_stream" "test_stream" {
  name             = "terraform-kinesis-test"
  shard_count      = 1
  retention_period = 48

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]

  tags = {
    Environment = "test"
  }
}

module "lambda_kinesis_to_kinesis_test" {
  source                 = "../lambda_kinesis_to_kinesis"
  lambda_name            = "lambda_kinesis_to_kinesis_test"
  lambda_handler         = "lambda_kinesis_to_kinesis_test.handler"
  aws_kinesis_stream_arn = aws_kinesis_stream.test_stream.arn
  lambda_environment_variables = {
    MY_ENV_VARIABLE = "my value"
  }
  lambda_service_subnets         = [aws_subnet.main.id]
  lambda_source_dir              = "lambdas/dummy"
  vpc_id                         = aws_vpc.main.id
  dead_letters_queue_name        = "kinesis_to_kinesis_processor_failures"
  event_source_starting_position = "LATEST"
  enable_consumer                = true
  kinesis_destination_arn        = aws_kinesis_stream.test_stream.arn
}