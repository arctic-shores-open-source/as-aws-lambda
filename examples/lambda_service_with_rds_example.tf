module "lambda_service_invoked_by_rds_example" {
  source         = "../lambda_service"
  lambda_name    = "rds_event_processor"
  lambda_handler = "example.handler"
  lambda_environment_variables = {
    MY_ENV_VARIABLE = "my value"
  }
  lambda_service_subnets = [aws_subnet.main.id]
  vpc_id                 = aws_vpc.main.id
}


module "rds_lambda_invoke" {
  source     = "../lambda_rds"
  lambda_arn = module.lambda_service_example.lambda_service_arn
  descriminator = "dev_my_app"
  db_instance_identifier = aws_db_instance.default.id
}

