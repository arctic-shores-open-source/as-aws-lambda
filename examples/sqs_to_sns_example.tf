module "sqs_to_sns_example" {
  source                 = "../sqs_to_sns"
  lambda_name            = "sqs_to_sns_forwarder_example"
  sns_topic_name         = "example_forwarding"
  sqs_queue_arn          = aws_sqs_queue.terraform_queue.arn
  sqs_queue_name         = "terraform-example-queue"
  sns_subject            = "This message originates from my terraform-example-queue"
  lambda_service_subnets = [aws_subnet.main.id]
  vpc_id                 = aws_vpc.main.id
}