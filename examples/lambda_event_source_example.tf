module "lambda_event_source_example" {
  source         = "../lambda_event_source"
  lambda_name    = "lambda_event_source_example"
  lambda_handler = "lambda_event_source_example.handler"
  lambda_environment_variables = {
    MY_ENV_VARIABLE = "my value"
  }
  lambda_service_subnets         = [aws_subnet.main.id]
  vpc_id                         = aws_vpc.main.id
  dynamo_table_arn               = aws_dynamodb_table.basic-dynamodb-table.arn
  dynamo_table_name              = aws_dynamodb_table.basic-dynamodb-table.name
  enable_cdc_stream              = false
  dead_letters_queue_name        = "my-dead-letters-queue"
  enable_consumer                = true
  aws_kinesis_stream_arn         = aws_kinesis_stream.test_stream.arn
  event_source_starting_position = "LATEST"
}

module "lambda_event_source_example_with_cdc_enabled" {
  source         = "../lambda_event_source"
  lambda_name    = "lambda_event_source_example"
  lambda_handler = "lambda_event_source_example.handler"
  lambda_environment_variables = {
    MY_ENV_VARIABLE = "my value"
  }
  lambda_service_subnets = [aws_subnet.main.id]
  vpc_id                 = aws_vpc.main.id
  dynamo_table_arn       = aws_dynamodb_table.basic-dynamodb-table.arn
  dynamo_table_name      = aws_dynamodb_table.basic-dynamodb-table.name
  // in this case, cdc_stream_name will be equal to $aws_dynamodb_table.basic-dynamodb-table.name
  enable_cdc_stream              = true
  dead_letters_queue_name        = "my-dead-letters-queue"
  enable_consumer                = true
  aws_kinesis_stream_arn         = aws_kinesis_stream.test_stream.arn
  event_source_starting_position = "LATEST"
}

module "lambda_event_source_example_with_cdc_enabled_and_name" {
  source         = "../lambda_event_source"
  lambda_name    = "lambda_event_source_example"
  lambda_handler = "lambda_event_source_example.handler"
  lambda_environment_variables = {
    MY_ENV_VARIABLE = "my value"
  }
  lambda_service_subnets         = [aws_subnet.main.id]
  vpc_id                         = aws_vpc.main.id
  dynamo_table_arn               = aws_dynamodb_table.basic-dynamodb-table.arn
  dynamo_table_name              = aws_dynamodb_table.basic-dynamodb-table.name
  enable_cdc_stream              = true
  cdc_stream_name                = "my-dynamo-kinesis-stream"
  dead_letters_queue_name        = "my-dead-letters-queue"
  enable_consumer                = true
  aws_kinesis_stream_arn         = aws_kinesis_stream.test_stream.arn
  event_source_starting_position = "LATEST"
}