module "lambda_writer_example" {
  source         = "../lambda_write"
  lambda_name    = "lambda_writer_example"
  lambda_handler = "lambda_writer_example.handler"
  lambda_environment_variables = {
    MY_ENV_VARIABLE = "my value"
  }
  lambda_service_subnets = [aws_subnet.main.id]
  vpc_id                 = aws_vpc.main.id
  dynamo_table_arn       = aws_dynamodb_table.basic-dynamodb-table.arn
  dynamo_table_name      = aws_dynamodb_table.basic-dynamodb-table.name
  enable_cdc_stream      = false
}

module "lambda_writer_example_with_cdc_enabled" {
  source         = "../lambda_write"
  lambda_name    = "lambda_writer_example"
  lambda_handler = "lambda_writer_example.handler"
  lambda_environment_variables = {
    MY_ENV_VARIABLE = "my value"
  }
  lambda_service_subnets = [aws_subnet.main.id]
  vpc_id                 = aws_vpc.main.id
  dynamo_table_arn       = aws_dynamodb_table.basic-dynamodb-table.arn
  dynamo_table_name      = aws_dynamodb_table.basic-dynamodb-table.name
  enable_cdc_stream      = true
  // in this case, cdc_stream_name will be equal to $aws_dynamodb_table.basic-dynamodb-table.name
}

module "lambda_writer_example_with_cdc_enabled_and_name" {
  source         = "../lambda_write"
  lambda_name    = "lambda_writer_example"
  lambda_handler = "lambda_writer_example.handler"
  lambda_environment_variables = {
    MY_ENV_VARIABLE = "my value"
  }
  lambda_service_subnets = [aws_subnet.main.id]
  vpc_id                 = aws_vpc.main.id
  dynamo_table_arn       = aws_dynamodb_table.basic-dynamodb-table.arn
  dynamo_table_name      = aws_dynamodb_table.basic-dynamodb-table.name
  enable_cdc_stream      = true
  cdc_stream_name        = "my-dynamo-db-stream-name"
}