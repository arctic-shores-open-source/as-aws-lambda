module "lambda_service_example" {
  source         = "../lambda_service"
  lambda_name    = "example"
  lambda_handler = "example.handler"
  lambda_environment_variables = {
    MY_ENV_VARIABLE = "my value"
  }
  lambda_service_subnets = [aws_subnet.main.id]
  vpc_id                 = aws_vpc.main.id
}