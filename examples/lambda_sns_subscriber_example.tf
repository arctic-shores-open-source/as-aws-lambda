module "user_updates_subscriber" {
  source         = "../lambda_sns"
  lambda_name    = "user_updates"
  lambda_handler = "user_updates.handler"
  lambda_environment_variables = {
    MY_ENV_VARIABLE = "my value"
  }
  lambda_service_subnets = [aws_subnet.main.id]
  vpc_id                 = aws_vpc.main.id
  sns_arn                = aws_sns_topic.user_updates.arn
}