
module "kinesis_processor" {
  source                             = "../lambda_kinesis"
  lambda_environment_variables       = var.lambda_environment_variables
  lambda_handler                     = var.lambda_handler
  lambda_name                        = var.lambda_name
  lambda_service_subnets             = var.lambda_service_subnets
  vpc_id                             = var.vpc_id
  aws_kinesis_stream_arn             = var.aws_kinesis_stream_arn
  bisect_batch_on_function_error     = var.bisect_batch_on_function_error
  dead_letters_queue_name            = var.dead_letters_queue_name
  enable_consumer                    = var.enable_consumer
  event_source_starting_at_timestamp = var.event_source_starting_at_timestamp
  event_source_starting_position     = var.event_source_starting_position
  maximum_batching_window_in_seconds = var.maximum_batching_window_in_seconds
  maximum_retry_attempts             = var.maximum_retry_attempts
  read_batch_size                    = var.read_batch_size
  lambda_source_dir                  = var.lambda_source_dir
  lambda_runtime                     = var.lambda_runtime
  security_group_id                  = var.security_group_id
  tags                               = var.tags
  timeout = var.timeout
}

resource "aws_iam_role_policy" "publish_to_kinesis" {
  name = "${module.kinesis_processor.lambda_service_function_name}_publish_to_kinesis"
  role = module.kinesis_processor.lambda_service_iam_id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
          "kinesis:PutRecord", "kinesis:PutRecords"
      ],
      "Resource": ["${var.kinesis_destination_arn}"]
    }
  ]
}
EOF
}