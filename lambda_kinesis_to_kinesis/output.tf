output "dead_letters_sns_queue" {
  value       = module.kinesis_processor.dead_letters_sns_queue
  description = "The arn of the SNS dead letters queue (contains event information of processing failures)."
}

output "dead_letters_sns_queue_id" {
  value       = module.kinesis_processor.dead_letters_sns_queue_id
  description = "The id of the SNS dead letters queue."
}

output "dead_letters_sns_queue_name" {
  value       = module.kinesis_processor.dead_letters_sns_queue_name
  description = "The name of the SNS dead letters queue"
}

output "lambda_service_security_group_id" {
  value       = module.kinesis_processor.lambda_service_security_group_id
  description = "The security group id for this lambda."
}

output "lambda_service_iam_arn" {
  value       = module.kinesis_processor.lambda_service_iam_arn
  description = "The iam role arn of the lamdba."
}

output "lambda_service_iam_id" {
  value       = module.kinesis_processor.lambda_service_iam_id
  description = "The id of the lamnda iam role."
}

output "lambda_service_function_name" {
  value       = module.kinesis_processor.lambda_service_function_name
  description = "The function name of the dummy lambda if it was created, otherwise null."
}

output "lambda_service_arn" {
  value       = module.kinesis_processor.lambda_service_arn
  description = "The lambda arn if it was created, otherwise null."
}


variable "security_group_id" {
  type        = string
  description = "A user defined security group for the lambda"
  default     = null
}