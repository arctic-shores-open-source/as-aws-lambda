## Description

Creates a lambda that consumes from an already created sqs queue using an event source mapping
and dropping failures in a newly created SNS topic acting as a dead letters queue.

<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_sqs_consumer"></a> [sqs\_consumer](#module\_sqs\_consumer) | ../lambda_service | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_iam_role_policy.publish_processing_failures](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy.sqs_read](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_lambda_event_source_mapping.sqs_consumer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_event_source_mapping) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_batch_size"></a> [batch\_size](#input\_batch\_size) | The maximum batch size before the lambda is invoked to get sqs messages | `number` | `10` | no |
| <a name="input_batch_window_in_seconds"></a> [batch\_window\_in\_seconds](#input\_batch\_window\_in\_seconds) | The maximum amount to wait before getting the next batch of sqs messages | `number` | `30` | no |
| <a name="input_enable_consumer"></a> [enable\_consumer](#input\_enable\_consumer) | Whether or not the consumer is enabled. Can be used for pausing stream consumption. | `bool` | `true` | no |
| <a name="input_lambda_environment_variables"></a> [lambda\_environment\_variables](#input\_lambda\_environment\_variables) | The environment variables to pass to the lambda runtime. | `map(any)` | `{}` | no |
| <a name="input_lambda_handler"></a> [lambda\_handler](#input\_lambda\_handler) | The lambda handler name. | `string` | n/a | yes |
| <a name="input_lambda_name"></a> [lambda\_name](#input\_lambda\_name) | The lambda name to be used for the middleware that forwards from sqs to sns. | `string` | n/a | yes |
| <a name="input_lambda_runtime"></a> [lambda\_runtime](#input\_lambda\_runtime) | The lambda runtime matching the source code provided in lambda\_source\_dir. | `string` | `"nodejs16.x"` | no |
| <a name="input_lambda_service_subnets"></a> [lambda\_service\_subnets](#input\_lambda\_service\_subnets) | The subnets to assign to the lambda for the vpc configuration. | `list(string)` | `[]` | no |
| <a name="input_lambda_source_dir"></a> [lambda\_source\_dir](#input\_lambda\_source\_dir) | The source directory where all the lambda code is ready to be zipped. If not provided, a dummy lambda is packaged | `string` | `null` | no |
| <a name="input_layers"></a> [layers](#input\_layers) | List of layer arn's to attach to the lambda. | `list(string)` | `[]` | no |
| <a name="input_security_group_id"></a> [security\_group\_id](#input\_security\_group\_id) | A user defined security group for the lambda | `string` | `null` | no |
| <a name="input_sqs_queue_arn"></a> [sqs\_queue\_arn](#input\_sqs\_queue\_arn) | The arn of the user managed sqs queue. | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | (Optional) Key-value map of resource tags to attach to all resources created by this module. | `map(any)` | `null` | no |
| <a name="input_timeout"></a> [timeout](#input\_timeout) | Amount of time your Lambda Function has to run in seconds. Max can be 900 seconds. Defaults to 3. | `number` | `3` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | The vpc id to assign to this lambda. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_lambda_service_arn"></a> [lambda\_service\_arn](#output\_lambda\_service\_arn) | The lambda arn if it was created, otherwise null. |
| <a name="output_lambda_service_function_name"></a> [lambda\_service\_function\_name](#output\_lambda\_service\_function\_name) | The function name of the dummy lambda if it was created, otherwise null. |
| <a name="output_lambda_service_iam_arn"></a> [lambda\_service\_iam\_arn](#output\_lambda\_service\_iam\_arn) | The iam role arn of the lamdba. |
| <a name="output_lambda_service_iam_id"></a> [lambda\_service\_iam\_id](#output\_lambda\_service\_iam\_id) | The id of the lamnda iam role. |
| <a name="output_lambda_service_security_group_id"></a> [lambda\_service\_security\_group\_id](#output\_lambda\_service\_security\_group\_id) | The security group id for this lambda. |
<!-- END_TF_DOCS -->