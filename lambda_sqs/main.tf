

module "sqs_consumer" {
  source                       = "../lambda_service"
  lambda_name                  = var.lambda_name
  lambda_handler               = var.lambda_handler
  lambda_environment_variables = var.lambda_environment_variables
  lambda_service_subnets       = var.lambda_service_subnets
  lambda_source_dir            = var.lambda_source_dir
  lambda_runtime               = var.lambda_runtime
  vpc_id                       = var.vpc_id
  security_group_id            = var.security_group_id
  tags                         = var.tags
  layers                       = var.layers
  timeout                      = var.timeout
}


resource "aws_iam_role_policy" "sqs_read" {
  name = "sqs_${module.sqs_consumer.lambda_service_function_name}"
  role = module.sqs_consumer.lambda_service_iam_id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
          "sqs:ReceiveMessage",
          "sqs:DeleteMessage",
          "sqs:GetQueueAttributes"
      ],
      "Resource": ["${var.sqs_queue_arn}"]
    }
  ]
}
EOF
}


resource "aws_iam_role_policy" "publish_processing_failures" {
  name = "${module.sqs_consumer.lambda_service_function_name}_publish_processing_failures"
  role = module.sqs_consumer.lambda_service_iam_id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
          "SQS:SendMessage"
      ],
      "Resource": ["${var.sqs_queue_arn}"]
    }
  ]
}
EOF
}

resource "aws_lambda_event_source_mapping" "sqs_consumer" {
  event_source_arn                   = var.sqs_queue_arn
  function_name                      = module.sqs_consumer.lambda_service_function_name
  maximum_batching_window_in_seconds = var.batch_window_in_seconds
  batch_size                         = var.batch_size

  enabled = var.enable_consumer

  depends_on = [
    aws_iam_role_policy.sqs_read,
    aws_iam_role_policy.publish_processing_failures,
  ]
}
