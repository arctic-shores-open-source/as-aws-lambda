## Description

Forward s3 notifications to an sqs queue
<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_s3_bucket_notification.bucket_notification](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_notification) | resource |
| [aws_sqs_queue.s3_events](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sqs_queue) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_filter_prefix"></a> [filter\_prefix](#input\_filter\_prefix) | Object key name prefix | `string` | `null` | no |
| <a name="input_filter_suffix"></a> [filter\_suffix](#input\_filter\_suffix) | Object key name suffix | `string` | `null` | no |
| <a name="input_redrive_policy"></a> [redrive\_policy](#input\_redrive\_policy) | Redrive policy for when the s3 event is undelivered from the sqs queue. | `string` | `null` | no |
| <a name="input_s3_bucket_id"></a> [s3\_bucket\_id](#input\_s3\_bucket\_id) | The name of the bucket (same bucket as the provided bucket arn) | `string` | n/a | yes |
| <a name="input_s3_events"></a> [s3\_events](#input\_s3\_events) | The s3 events to listen to | `list(string)` | <pre>[<br>  "s3:ObjectCreated:*"<br>]</pre> | no |
| <a name="input_s3_events_sqs_name"></a> [s3\_events\_sqs\_name](#input\_s3\_events\_sqs\_name) | The name of the sqs queue to receive events from s3 | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | (Optional) Key-value map of resource tags to attach to all resources created by this module. | `map(any)` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_sqs_arn"></a> [sqs\_arn](#output\_sqs\_arn) | The arn of the sqs receiving the s3 events |
<!-- END_TF_DOCS -->