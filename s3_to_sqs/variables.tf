variable "s3_events_sqs_name" {
  type        = string
  description = "The name of the sqs queue to receive events from s3"
}

variable "filter_prefix" {
  type        = string
  description = "Object key name prefix"
  default     = null
}

variable "filter_suffix" {
  type        = string
  description = "Object key name suffix"
  default     = null
}

variable "s3_events" {
  type        = list(string)
  description = "The s3 events to listen to"
  default     = ["s3:ObjectCreated:*"]
}

variable "s3_bucket_id" {
  type        = string
  description = "The name of the bucket (same bucket as the provided bucket arn)"
}

variable "redrive_policy" {
  type        = string
  description = "Redrive policy for when the s3 event is undelivered from the sqs queue."
  default     = null
}

variable "tags" {
  type        = map(any)
  description = "(Optional) Key-value map of resource tags to attach to all resources created by this module."
  default     = null
}