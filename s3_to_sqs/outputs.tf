output "sqs_arn" {
  description = "The arn of the sqs receiving the s3 events"
  value       = aws_sqs_queue.s3_events.arn
}