resource "aws_sqs_queue" "s3_events" {
  name           = var.s3_events_sqs_name
  tags           = var.tags
  policy         = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": "*",
      "Action": "sqs:SendMessage",
      "Resource": "arn:aws:sqs:*:*:${var.s3_events_sqs_name}",
      "Condition": {
        "ArnEquals": { "aws:SourceArn": "arn:aws:s3:::${var.s3_bucket_id}" }
      }
    }
  ]
}
POLICY
  redrive_policy = var.redrive_policy
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = var.s3_bucket_id

  queue {
    queue_arn     = aws_sqs_queue.s3_events.arn
    events        = var.s3_events
    filter_suffix = var.filter_suffix
    filter_prefix = var.filter_prefix
  }
  depends_on = [
    aws_sqs_queue.s3_events
  ]
}