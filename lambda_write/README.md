## lambda_write

A lambda that persists to a dynamo table. 

This module is opinionated for the write 
part of a potential CQRS implementation in that we should write to a dynamo table
and read any dynamo events from a kinesis stream (enable via config).

The dynamo table is user defined because it depends on user defined code.
<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 2.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 2.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_persist_commands"></a> [persist\_commands](#module\_persist\_commands) | ../lambda_service | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_dynamodb_kinesis_streaming_destination.persist_commands_ack](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_kinesis_streaming_destination) | resource |
| [aws_iam_role_policy.command_persistency_to_dynamo](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_kinesis_stream.persist_commands_ack](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kinesis_stream) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cdc_stream_name"></a> [cdc\_stream\_name](#input\_cdc\_stream\_name) | The change data capture stream name to get dynamo state change events. | `string` | `""` | no |
| <a name="input_commands_ack_stream_shards"></a> [commands\_ack\_stream\_shards](#input\_commands\_ack\_stream\_shards) | The number of shards for the dynamo kinesis stream | `number` | `1` | no |
| <a name="input_dynamo_table_arn"></a> [dynamo\_table\_arn](#input\_dynamo\_table\_arn) | The dynamo table arn that this lambda will be using | `string` | n/a | yes |
| <a name="input_dynamo_table_name"></a> [dynamo\_table\_name](#input\_dynamo\_table\_name) | The dynamo table name that this lambda will be using (same table as the provided arn) | `string` | n/a | yes |
| <a name="input_enable_cdc_stream"></a> [enable\_cdc\_stream](#input\_enable\_cdc\_stream) | Flag whether to capture item level modifications in dynamo db | `bool` | `false` | no |
| <a name="input_lambda_environment_variables"></a> [lambda\_environment\_variables](#input\_lambda\_environment\_variables) | The environment variables to pass to the lambda runtime. | `map(any)` | `{}` | no |
| <a name="input_lambda_handler"></a> [lambda\_handler](#input\_lambda\_handler) | The lambda handler name. | `string` | n/a | yes |
| <a name="input_lambda_name"></a> [lambda\_name](#input\_lambda\_name) | The name of the lambda to construct | `string` | n/a | yes |
| <a name="input_lambda_runtime"></a> [lambda\_runtime](#input\_lambda\_runtime) | The lambda runtime matching the source code provided in lambda\_source\_dir. | `string` | `"nodejs16.x"` | no |
| <a name="input_lambda_service_subnets"></a> [lambda\_service\_subnets](#input\_lambda\_service\_subnets) | The subnets to assign to the lambda for the vpc configuration. | `list(string)` | `[]` | no |
| <a name="input_lambda_source_dir"></a> [lambda\_source\_dir](#input\_lambda\_source\_dir) | The source directory where all the lambda code is ready to be zipped. If not provided, a dummy lambda is packaged | `string` | `null` | no |
| <a name="input_security_group_id"></a> [security\_group\_id](#input\_security\_group\_id) | A user defined security group for the lambda | `string` | `null` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | (Optional) Key-value map of resource tags to attach to all resources created by this module. | `map(any)` | `null` | no |
| <a name="input_timeout"></a> [timeout](#input\_timeout) | Amount of time your Lambda Function has to run in seconds. Max can be 900 seconds. Defaults to 3. | `number` | `3` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | The vpc id to assign to this lambda. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_lambda_service_arn"></a> [lambda\_service\_arn](#output\_lambda\_service\_arn) | The lambda arn if it was created, otherwise null. |
| <a name="output_lambda_service_function_name"></a> [lambda\_service\_function\_name](#output\_lambda\_service\_function\_name) | The function name of the dummy lambda if it was created, otherwise null. |
| <a name="output_lambda_service_iam_arn"></a> [lambda\_service\_iam\_arn](#output\_lambda\_service\_iam\_arn) | The iam role arn of the lamdba. |
| <a name="output_lambda_service_iam_id"></a> [lambda\_service\_iam\_id](#output\_lambda\_service\_iam\_id) | The id of the lamnda iam role. |
| <a name="output_lambda_service_security_group_id"></a> [lambda\_service\_security\_group\_id](#output\_lambda\_service\_security\_group\_id) | The security group id for this lambda. |
| <a name="output_persist_commands_ack_stream_arn"></a> [persist\_commands\_ack\_stream\_arn](#output\_persist\_commands\_ack\_stream\_arn) | n/a |
<!-- END_TF_DOCS -->