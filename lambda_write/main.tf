locals {
  with_cdc_stream_name = var.enable_cdc_stream && var.cdc_stream_name == "" ? var.dynamo_table_name : var.cdc_stream_name
}

module "persist_commands" {
  source                       = "../lambda_service"
  lambda_name                  = var.lambda_name
  lambda_handler               = var.lambda_handler
  lambda_environment_variables = var.lambda_environment_variables
  lambda_service_subnets       = var.lambda_service_subnets
  vpc_id                       = var.vpc_id
  lambda_source_dir            = var.lambda_source_dir
  lambda_runtime               = var.lambda_runtime
  security_group_id            = var.security_group_id
  tags                         = var.tags
  timeout                      = var.timeout
}

resource "aws_iam_role_policy" "command_persistency_to_dynamo" {
  name = "${var.lambda_name}_command_persistency_to_dynamo"
  role = module.persist_commands.lambda_service_iam_id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
          "dynamodb:PutItem"
      ],
      "Resource": ["${var.dynamo_table_arn}"]
    }
  ]
}
EOF
}

resource "aws_kinesis_stream" "persist_commands_ack" {
  count       = var.enable_cdc_stream ? 1 : 0
  name        = local.with_cdc_stream_name
  shard_count = 1
  tags        = var.tags
}

resource "aws_dynamodb_kinesis_streaming_destination" "persist_commands_ack" {
  count      = var.enable_cdc_stream ? 1 : 0
  stream_arn = aws_kinesis_stream.persist_commands_ack[count.index].arn
  table_name = var.dynamo_table_name
}