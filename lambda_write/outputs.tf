output "lambda_service_security_group_id" {
  value       = module.persist_commands.lambda_service_security_group_id
  description = "The security group id for this lambda."
}

output "lambda_service_iam_arn" {
  value       = module.persist_commands.lambda_service_iam_arn
  description = "The iam role arn of the lamdba."
}

output "lambda_service_iam_id" {
  value       = module.persist_commands.lambda_service_iam_id
  description = "The id of the lamnda iam role."
}

output "lambda_service_function_name" {
  value       = module.persist_commands.lambda_service_function_name
  description = "The function name of the dummy lambda if it was created, otherwise null."
}

output "lambda_service_arn" {
  value       = module.persist_commands.lambda_service_arn
  description = "The lambda arn if it was created, otherwise null."
}

output "persist_commands_ack_stream_arn" {
  value = aws_kinesis_stream.persist_commands_ack.*.arn
}

