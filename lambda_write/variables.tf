variable "dynamo_table_arn" {
  type        = string
  description = "The dynamo table arn that this lambda will be using"
}

/*
    Due to limitations in how terraform module and transitively the aws cli works
    with aws_dynamodb_kinesis_streaming_destination, we need both name and arn
*/
variable "dynamo_table_name" {
  type        = string
  description = "The dynamo table name that this lambda will be using (same table as the provided arn)"
}

variable "lambda_name" {
  type        = string
  description = "The name of the lambda to construct"
}

variable "lambda_environment_variables" {
  type        = map(any)
  default     = {}
  description = "The environment variables to pass to the lambda runtime."
}

variable "lambda_service_subnets" {
  type        = list(string)
  default     = []
  description = "The subnets to assign to the lambda for the vpc configuration."
}

variable "lambda_handler" {
  type        = string
  description = "The lambda handler name."
}

variable "enable_cdc_stream" {
  type        = bool
  default     = false
  description = "Flag whether to capture item level modifications in dynamo db"
}

variable "cdc_stream_name" {
  type        = string
  default     = ""
  description = "The change data capture stream name to get dynamo state change events."
}

variable "vpc_id" {
  type        = string
  description = "The vpc id to assign to this lambda."
}

variable "commands_ack_stream_shards" {
  type        = number
  description = "The number of shards for the dynamo kinesis stream"
  default     = 1
}

variable "lambda_source_dir" {
  type        = string
  description = "The source directory where all the lambda code is ready to be zipped. If not provided, a dummy lambda is packaged"
  default     = null
}

variable "lambda_runtime" {
  type        = string
  description = "The lambda runtime matching the source code provided in lambda_source_dir."
  default     = "nodejs16.x"
}

variable "security_group_id" {
  type        = string
  description = "A user defined security group for the lambda"
  default     = null
}

variable "tags" {
  type        = map(any)
  description = "(Optional) Key-value map of resource tags to attach to all resources created by this module."
  default     = null
}

variable "timeout" {
  type        = number
  description = "Amount of time your Lambda Function has to run in seconds. Max can be 900 seconds. Defaults to 3."
  default     = 3
}