

resource "aws_sns_topic" "to_forward" {
  name = var.sns_topic_name
  tags = var.tags
}

module "sqs_to_sns_lambda" {
  source         = "../lambda_service"
  lambda_name    = var.lambda_name
  lambda_handler = "sqs_to_sns.handler"
  lambda_environment_variables = {
    SNS_SUBJECT = var.sns_subject
    SNS_ARN     = aws_sns_topic.to_forward.arn
  }
  lambda_service_subnets = var.lambda_service_subnets
  lambda_source_dir      = "${path.module}/lambda_code"
  vpc_id                 = var.vpc_id
  security_group_id      = var.security_group_id
  tags                   = var.tags
}


resource "aws_iam_role_policy" "sqs_read" {
  name = "sqs_${module.sqs_to_sns_lambda.lambda_service_function_name}"
  role = module.sqs_to_sns_lambda.lambda_service_iam_id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
          "sqs:ReceiveMessage",
          "sqs:DeleteMessage",
          "sqs:GetQueueAttributes"
      ],
      "Resource": ["${var.sqs_queue_arn}"]
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "publish_to_sns" {
  name = "${module.sqs_to_sns_lambda.lambda_service_function_name}_publish_to_sns_${var.sns_topic_name}"
  role = module.sqs_to_sns_lambda.lambda_service_iam_id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
          "SNS:Publish"
      ],
      "Resource": ["${aws_sns_topic.to_forward.arn}"]
    }
  ]
}
EOF
}


resource "aws_lambda_event_source_mapping" "sns_event_source_mapping" {
  event_source_arn                   = var.sqs_queue_arn
  function_name                      = module.sqs_to_sns_lambda.lambda_service_function_name
  maximum_batching_window_in_seconds = var.batch_window_in_seconds
  batch_size                         = var.batch_size
}

