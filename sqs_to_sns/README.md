## sqs_to_sns

Creates a new sns queue that has a source an existing sqs queue.
<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_sqs_to_sns_lambda"></a> [sqs\_to\_sns\_lambda](#module\_sqs\_to\_sns\_lambda) | ../lambda_service | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_iam_role_policy.publish_to_sns](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy.sqs_read](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_lambda_event_source_mapping.sns_event_source_mapping](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_event_source_mapping) | resource |
| [aws_sns_topic.to_forward](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_batch_size"></a> [batch\_size](#input\_batch\_size) | The maximum batch size before the lambda is invoked to get sqs messages | `number` | `10` | no |
| <a name="input_batch_window_in_seconds"></a> [batch\_window\_in\_seconds](#input\_batch\_window\_in\_seconds) | The maximum amount to wait before getting the next batch of sqs messages | `number` | `30` | no |
| <a name="input_lambda_name"></a> [lambda\_name](#input\_lambda\_name) | The lambda name to be used for the middleware that forwards from sqs to sns. | `string` | n/a | yes |
| <a name="input_lambda_runtime"></a> [lambda\_runtime](#input\_lambda\_runtime) | The lambda runtime matching the source code provided in lambda\_source\_dir. | `string` | `"nodejs16.x"` | no |
| <a name="input_lambda_service_subnets"></a> [lambda\_service\_subnets](#input\_lambda\_service\_subnets) | The subnets to assign to the lambda for the vpc configuration. | `list(string)` | `[]` | no |
| <a name="input_security_group_id"></a> [security\_group\_id](#input\_security\_group\_id) | A user defined security group for the lambda | `string` | `null` | no |
| <a name="input_sns_subject"></a> [sns\_subject](#input\_sns\_subject) | The subject of the sns message when forwarded from sqs. | `string` | n/a | yes |
| <a name="input_sns_topic_name"></a> [sns\_topic\_name](#input\_sns\_topic\_name) | The name of the sns topic to create for pushing sqs messages | `string` | n/a | yes |
| <a name="input_sqs_queue_arn"></a> [sqs\_queue\_arn](#input\_sqs\_queue\_arn) | The arn of the user managed sqs queue. | `string` | n/a | yes |
| <a name="input_sqs_queue_name"></a> [sqs\_queue\_name](#input\_sqs\_queue\_name) | The name of the sqs queue to be used to identify the lambda that pushes the messages to sns. | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | (Optional) Key-value map of resource tags to attach to all resources created by this module. | `map(any)` | `null` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | The vpc id to assign to this lambda. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_forwarding_sns_arn"></a> [forwarding\_sns\_arn](#output\_forwarding\_sns\_arn) | The sns topic that sqs messages are forwarded to. |
<!-- END_TF_DOCS -->