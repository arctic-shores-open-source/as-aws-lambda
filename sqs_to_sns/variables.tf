variable "lambda_name" {
  type        = string
  description = "The lambda name to be used for the middleware that forwards from sqs to sns."
}
variable "sns_topic_name" {
  type        = string
  description = "The name of the sns topic to create for pushing sqs messages"
}

variable "sqs_queue_arn" {
  type        = string
  description = "The arn of the user managed sqs queue."
}


variable "sqs_queue_name" {
  type        = string
  description = "The name of the sqs queue to be used to identify the lambda that pushes the messages to sns."
}

variable "sns_subject" {
  type        = string
  description = "The subject of the sns message when forwarded from sqs."
}

variable "vpc_id" {
  type        = string
  description = "The vpc id to assign to this lambda."
}

variable "lambda_service_subnets" {
  type        = list(string)
  default     = []
  description = "The subnets to assign to the lambda for the vpc configuration."
}

variable "lambda_runtime" {
  type        = string
  description = "The lambda runtime matching the source code provided in lambda_source_dir."
  default     = "nodejs16.x"
}

variable "batch_size" {
  type        = number
  description = "The maximum batch size before the lambda is invoked to get sqs messages"
  default     = 10
}

variable "batch_window_in_seconds" {
  type        = number
  description = "The maximum amount to wait before getting the next batch of sqs messages"
  default     = 30
}

variable "security_group_id" {
  type        = string
  description = "A user defined security group for the lambda"
  default     = null
}

variable "tags" {
  type        = map(any)
  description = "(Optional) Key-value map of resource tags to attach to all resources created by this module."
  default     = null
}