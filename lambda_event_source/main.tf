
locals {
  with_cdc_stream_name = var.enable_cdc_stream && var.cdc_stream_name == "" ? var.dynamo_table_name : var.cdc_stream_name
}

module "lambda_processor" {
  source                       = "../lambda_write"
  dynamo_table_arn             = var.dynamo_table_arn
  dynamo_table_name            = var.dynamo_table_name
  lambda_environment_variables = var.lambda_environment_variables
  lambda_handler               = var.lambda_handler
  lambda_name                  = var.lambda_name
  lambda_service_subnets       = var.lambda_service_subnets
  vpc_id                       = var.vpc_id
  enable_cdc_stream            = var.enable_cdc_stream
  cdc_stream_name              = local.with_cdc_stream_name
  commands_ack_stream_shards   = var.commands_ack_stream_shards
  lambda_source_dir            = var.lambda_source_dir
  lambda_runtime               = var.lambda_runtime
  security_group_id            = var.security_group_id
  tags                         = var.tags
  timeout                      = var.timeout
}

resource "aws_lambda_event_source_mapping" "kinesis_to_lambda" {
  event_source_arn                   = var.aws_kinesis_stream_arn
  function_name                      = module.lambda_processor.lambda_service_function_name
  starting_position                  = var.event_source_starting_position
  starting_position_timestamp        = var.event_source_starting_at_timestamp
  batch_size                         = var.read_batch_size
  bisect_batch_on_function_error     = var.bisect_batch_on_function_error
  enabled                            = var.enable_consumer
  maximum_batching_window_in_seconds = var.maximum_batching_window_in_seconds
  maximum_retry_attempts             = var.maximum_retry_attempts
  destination_config {
    on_failure {
      destination_arn = aws_sns_topic.processing_failures.arn
    }
  }
}

resource "aws_iam_role_policy" "kinesis_to_lambda_read" {
  name = "${module.lambda_processor.lambda_service_function_name}_read_kinesis"
  role = module.lambda_processor.lambda_service_iam_id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
          "kinesis:GetRecords",
          "kinesis:GetShardIterator",
          "kinesis:DescribeStream",
          "kinesis:ListShards",
          "kinesis:ListStreams"
      ],
      "Resource": ["${var.aws_kinesis_stream_arn}"]
    }
  ]
}
EOF
}

resource "aws_sns_topic" "processing_failures" {
  name = var.dead_letters_queue_name
  tags = var.tags
}

resource "aws_iam_role_policy" "publish_processing_failures" {
  name = "${module.lambda_processor.lambda_service_function_name}_publish_processing_failures"
  role = module.lambda_processor.lambda_service_iam_id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
          "SNS:Publish"
      ],
      "Resource": ["${aws_sns_topic.processing_failures.arn}"]
    }
  ]
}
EOF
}