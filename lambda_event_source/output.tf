output "dead_letters_sns_queue" {
  value       = aws_sns_topic.processing_failures.arn
  description = "The arn of the SNS dead letters queue (contains event information of processing failures)."
}

output "dead_letters_sns_queue_id" {
  value       = aws_sns_topic.processing_failures.id
  description = "The id of the SNS dead letters queue."
}

output "dead_letters_sns_queue_name" {
  value       = aws_sns_topic.processing_failures.name
  description = "The name of the SNS dead letters queue"
}

output "lambda_service_security_group_id" {
  value       = module.lambda_processor.lambda_service_security_group_id
  description = "The security group id for this lambda."
}

output "lambda_service_iam_arn" {
  value       = module.lambda_processor.lambda_service_iam_arn
  description = "The iam role arn of the lamdba."
}

output "lambda_service_iam_id" {
  value       = module.lambda_processor.lambda_service_iam_id
  description = "The id of the lamnda iam role."
}

output "lambda_service_function_name" {
  value       = module.lambda_processor.lambda_service_function_name
  description = "The function name of the dummy lambda if it was created, otherwise null."
}

output "lambda_service_arn" {
  value       = module.lambda_processor.lambda_service_arn
  description = "The lambda arn if it was created, otherwise null."
}

output "persist_commands_ack_stream_arn" {
  value = module.lambda_processor.persist_commands_ack_stream_arn
}
