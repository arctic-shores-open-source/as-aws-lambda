

module "forward_s3_events_to_sqs" {
  source             = "../s3_to_sqs"
  s3_events_sqs_name = var.s3_events_sqs_name
  filter_prefix      = var.filter_prefix
  filter_suffix      = var.filter_suffix
  s3_events          = var.s3_events
  s3_bucket_id       = var.s3_bucket_id
  tags               = var.tags
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.processing_failures.arn
    maxReceiveCount     = 2
  })
}

module "s3_events_lambda_handler" {
  source                       = "../lambda_sqs"
  lambda_environment_variables = var.lambda_environment_variables
  lambda_handler               = var.lambda_handler
  lambda_name                  = var.lambda_name
  lambda_service_subnets       = var.lambda_service_subnets
  vpc_id                       = var.vpc_id
  lambda_source_dir            = var.lambda_source_dir
  lambda_runtime               = var.lambda_runtime
  security_group_id            = var.security_group_id
  batch_size                   = var.batch_size
  batch_window_in_seconds      = var.batch_window_in_seconds
  sqs_queue_arn                = module.forward_s3_events_to_sqs.sqs_arn
  enable_consumer              = var.enable_consumer
  tags                         = var.tags
  timeout                      = var.timeout
  depends_on = [
    module.forward_s3_events_to_sqs
  ]
}

resource "aws_sqs_queue" "processing_failures" {
  name = var.dead_letters_queue_name
  tags = var.tags
}

module "forward_dead_letters" {
  source                 = "../sqs_to_sns"
  lambda_name            = var.dead_letters_lambda_name
  sns_topic_name         = var.dead_letters_queue_name
  sqs_queue_arn          = aws_sqs_queue.processing_failures.arn
  sqs_queue_name         = var.dead_letters_queue_name
  sns_subject            = var.failure_subject
  lambda_service_subnets = var.lambda_service_subnets
  vpc_id                 = var.vpc_id
  depends_on             = [aws_sqs_queue.processing_failures]
  tags                   = var.tags
}