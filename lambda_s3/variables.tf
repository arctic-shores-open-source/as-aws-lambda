variable "lambda_name" {
  type        = string
  description = "The name of the lambda to construct"
}

variable "lambda_environment_variables" {
  type        = map(any)
  default     = {}
  description = "The environment variables to pass to the lambda runtime."
}

variable "lambda_service_subnets" {
  type        = list(string)
  default     = []
  description = "The subnets to assign to the lambda for the vpc configuration."
}

variable "lambda_handler" {
  type        = string
  description = "The lambda handler name."
}

variable "vpc_id" {
  type        = string
  description = "The vpc id to assign to this lambda."
}

variable "lambda_source_dir" {
  type        = string
  description = "The source directory where all the lambda code is ready to be zipped. If not provided, a dummy lambda is packaged"
  default     = null
}

variable "lambda_runtime" {
  type        = string
  description = "The lambda runtime matching the source code provided in lambda_source_dir."
  default     = "nodejs16.x"
}

variable "security_group_id" {
  type        = string
  description = "A user defined security group for the lambda"
  default     = null
}

variable "filter_prefix" {
  type        = string
  description = "Object key name prefix"
  default     = null
}

variable "filter_suffix" {
  type        = string
  description = "Object key name suffix"
  default     = null
}

variable "s3_events" {
  type        = list(string)
  description = "The s3 events to listen to"
  default     = ["s3:ObjectCreated:*"]
}

variable "s3_bucket_id" {
  type        = string
  description = "The name of the bucket (same bucket as the provided bucket arn)"
}

variable "s3_events_sqs_name" {
  type        = string
  description = "The name of the sqs queue to forward s3 events to"
}

variable "enable_consumer" {
  type        = bool
  description = "Whether or not the consumer is enabled. Can be used for pausing stream consumption."
  default     = true
}

variable "dead_letters_queue_name" {
  type        = string
  description = "The name of the dead letters queue to receive processing failure information."
}

variable "batch_size" {
  type        = number
  description = "The maximum batch size before the lambda is invoked to get sqs messages"
  default     = 10
}

variable "batch_window_in_seconds" {
  type        = number
  description = "The maximum amount to wait before getting the next batch of sqs messages"
  default     = 30
}

variable "failure_subject" {
  type        = string
  description = "The title when a failure is reported to dead letters."
}

variable "dead_letters_lambda_name" {
  type        = string
  description = "The name of the lambda to process dead letters"
}

variable "tags" {
  type        = map(any)
  description = "(Optional) Key-value map of resource tags to attach to all resources created by this module."
  default     = null
}

variable "timeout" {
  type        = number
  description = "Amount of time your Lambda Function has to run in seconds. Max can be 900 seconds. Defaults to 3."
  default     = 3
}