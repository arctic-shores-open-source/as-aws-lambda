variable "lambda_name" {
  type        = string
  description = "The name of the lambda to construct"
}

variable "lambda_environment_variables" {
  type        = map(any)
  default     = {}
  description = "The environment variables to pass to the lambda runtime."
}

variable "lambda_service_subnets" {
  type        = list(string)
  default     = []
  description = "The subnets to assign to the lambda for the vpc configuration."
}

variable "lambda_handler" {
  type        = string
  description = "The lambda handler name."
}

variable "vpc_id" {
  type        = string
  description = "The vpc id to assign to this lambda."
}

variable "lambda_source_dir" {
  type        = string
  description = "The source directory where all the lambda code is ready to be zipped. If not provided, a dummy lambda is packaged"
  default     = null
}

variable "lambda_runtime" {
  type        = string
  description = "The lambda runtime matching the source code provided in lambda_source_dir."
  default     = "nodejs16.x"
}

variable "sns_arn" {
  type        = string
  description = "The SNS ARN for the new lambda to subscribe to"
}

variable "security_group_id" {
  type        = string
  description = "A user defined security group for the lambda"
  default     = null
}

variable "tags" {
  type        = map(any)
  description = "(Optional) Key-value map of resource tags to attach to all resources created by this module."
  default     = null
}

variable "timeout" {
  type        = number
  description = "Amount of time your Lambda Function has to run in seconds. Max can be 900 seconds. Defaults to 3."
  default     = 3
}