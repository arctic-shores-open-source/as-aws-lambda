output "lambda_security_group_id" {
  value       = module.sns_subscriber.lambda_service_security_group_id
  description = "The security group id for this lambda."
}

output "lambda_iam_arn" {
  value       = module.sns_subscriber.lambda_service_iam_arn
  description = "The iam role arn of the lamdba."
}

output "lambda_iam_id" {
  value       = module.sns_subscriber.lambda_service_iam_id
  description = "The id of the lamnda iam role."
}

output "lambda_function_name" {
  value       = module.sns_subscriber.lambda_service_function_name
  description = "The function name of the lambda that was created"
}

output "lambda_arn" {
  value       = module.sns_subscriber.lambda_service_arn
  description = "The arn of the lambda that was created."
}